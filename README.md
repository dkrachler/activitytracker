# Activity Tracker

To use this App register a new user with your email, if do not want to sign up you can
use this demo user:

````
Username: john.doe@acme.com
Password: 123456
````

Make sure that you´re conected to the Internet, otherwise you will not be able to login.

The Application has been tested with following devices:

- Samsung Galaxy A3 (2016) - Android 7.0
- Samsung S7 Edge - Android 7.0
- Samsung S7 - Android 7.0
