package mscddb.com.activitytracker.activity;


import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.ViewUtils;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import mscddb.com.activitytracker.R;
import mscddb.com.activitytracker.model.WayPoint;

public class DetailViewActivity extends AppCompatActivity implements OnMapReadyCallback {

    TextView distanceTxt, durationTxt, maxSpeedTxt, speedTxt;

    private FirebaseUser currentUser;
    public FirebaseAuth.AuthStateListener authListener;
    public ValueEventListener mActivityListener;

    private GoogleMap mMap;
    private List<LatLng> points;

    public DatabaseReference mActivityReference;
    public mscddb.com.activitytracker.model.Activity activity;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.delete_button) {
            deleteActivity();
        }

        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        currentUser = user;

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    startActivity(new Intent(DetailViewActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };


        distanceTxt = findViewById(R.id.distanceDetailTxt);
        durationTxt = findViewById(R.id.durationDetailTxt);
        maxSpeedTxt = findViewById(R.id.maxSpeedDetailTxt);

        Intent intent = this.getIntent();
        String activity_id = intent.getExtras().getString("id");

        mActivityReference = FirebaseDatabase.getInstance().getReference()
                .child("android").child(currentUser.getUid()).child("activities").child(activity_id);

        points = new ArrayList<>();

    }

    public void deleteActivity(){

        if (mActivityReference != null) {
            mActivityReference.removeEventListener(mActivityListener);
        }

        Intent intent = this.getIntent();
        String activity_id = intent.getExtras().getString("id");

        mActivityReference = FirebaseDatabase.getInstance().getReference()
                .child("android").child(currentUser.getUid()).child("activities").child(activity_id);

        mActivityReference.setValue(null);

        finish();

    }

    private boolean initializeMap() {
        if (mMap == null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mMap = googleMap;
                }
            });
        }
        return (mMap != null);
    }

    @Override
    public void onStart() {
        super.onStart();

        initializeMap();

        ValueEventListener activityListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                activity = new mscddb.com.activitytracker.model.Activity();

                mscddb.com.activitytracker.model.Activity single_activity = dataSnapshot.getValue(mscddb.com.activitytracker.model.Activity.class);
                activity = single_activity;

                distanceTxt = findViewById(R.id.distanceDetailTxt);
                durationTxt = findViewById(R.id.durationDetailTxt);
                speedTxt = findViewById(R.id.speedDetailTxt);
                maxSpeedTxt = findViewById(R.id.maxSpeedDetailTxt);

                

                String distance = String.valueOf(String.format("%.2f", activity.getDistance()));
                long secs = activity.getDuration() / 1000;
                double avgSpeed = (secs > 0)? activity.getDistance() * 1000 / secs * 3.6 : 0;

                String time = String.format("%02d:%02d:%02d", (secs % 86400) / 3600, (secs % 3600) / 60, secs % 60);
                String avg_speed = String.valueOf(String.format("%.2f", avgSpeed));
                String max_speed = String.valueOf(String.format("%.2f", activity.getMax_speed()));

                distanceTxt.setText(distance + " km");
                durationTxt.setText(time);
                speedTxt.setText(avg_speed + " km/h");
                maxSpeedTxt.setText(max_speed + " km/h");


                final List<WayPoint> waypoints = activity.getWaypoints();
                final LatLngBounds.Builder builder = new LatLngBounds.Builder();

                if(waypoints != null) {

                    for (WayPoint waypoint : waypoints) {
                        points.add(new LatLng(waypoint.getLatitude(), waypoint.getLongitude()));

                        builder.include(new LatLng(waypoint.getLatitude(), waypoint.getLongitude()));

                        if (points.size() > 2) {

                            if (initializeMap()) {

                                drawPath(points);
                            }
                        }
                    }


                    mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                        @Override
                        public void onMapLoaded() {
                            if (initializeMap() && points.size() == waypoints.size()) {
                                LatLngBounds bounds = builder.build();
                                int padding = 50; // offset from edges of the map in pixels
                                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                                mMap.animateCamera(cu, 1000, null);
                            }
                        }
                    });

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        mActivityReference.addValueEventListener(activityListener);
        mActivityListener = activityListener;

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        LatLng cLoc = new LatLng(points.get(0).latitude, points.get(0).longitude);

        CameraUpdate center = CameraUpdateFactory.newLatLng(cLoc);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);

        // mMap.moveCamera(center);
        // mMap.animateCamera(zoom);

        if (points.size() > 2) {

            if (initializeMap()) {

                // mMap.moveCamera(center);
                // mMap.animateCamera(zoom);

                drawPath(points);
            }
        }

    }

    @Override
    public void onStop(){
        super.onStop();
        if (mActivityReference != null) {
            mActivityReference.removeEventListener(mActivityListener);
        }

    }

    public void drawPath(List<LatLng> list) {
        if (initializeMap()) {
            mMap.addPolyline(new PolylineOptions()
                    .addAll(list)
                    .width(10)
                    .color(Color.parseColor("#05b1fb"))
                    .geodesic(true)
            );
        }
    }

}
