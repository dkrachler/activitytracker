package mscddb.com.activitytracker.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import mscddb.com.activitytracker.R;
import mscddb.com.activitytracker.activity.DetailViewActivity;
import mscddb.com.activitytracker.model.Activity;
import mscddb.com.activitytracker.model.WayPoint;

/**
 * Created by davidkrachler on 01.02.18.
 */

public class ActivityAdapter extends BaseAdapter{

    private Context context;
    private List activities;

    public ActivityAdapter(Context context) {
        this.context = context;
    }

    public void setActivities(List<Activity> activities){
        this.activities = activities;
    }

    @Override
    public int getCount() {
        if (activities == null) {
            return 0;
        }
        return activities.size();
    }

    @Override
    public Object getItem(int i) {
        if (activities == null) {
            return 0;
        }
        return activities.get(i);
    }

    @Override
    public long getItemId(int i) {
        if (activities == null || activities.size() == 0) {
            return 0;
        }
        return activities.get(i).hashCode();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.view_list_item, viewGroup, false);
        }

        final Activity activity = (Activity) getItem(i);

        if (activity != null) {

            long unixSeconds = activity.getTimestamp();
            Date date = new Date(unixSeconds*1000L);

            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            String formattedDate = sdf.format(date);

            TextView date_of_activity = view.findViewById(R.id.date);
            date_of_activity.setText(String.valueOf(formattedDate));

            long secs = activity.getDuration() / 1000;
            String time = String.format("%02d:%02d:%02d", (secs % 86400) / 3600, (secs % 3600) / 60, secs % 60);

            TextView duration = view.findViewById(R.id.duration);
            duration.setText(String.valueOf(time));

            TextView distance = view.findViewById(R.id.distance);
            distance.setText(String.valueOf(String.format("%.2f", activity.getDistance())) + " km");

        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                long secs = activity.getDuration() / 1000;
                String time = String.format("%02d:%02d:%02d", (secs % 86400) / 3600, (secs % 3600) / 60, secs % 60);

                Map id = activity.toMap();
                String activityId = activity.getId();

                openDetailActivity(String.valueOf( String.format("%.2f", activity.getDistance())) + " km", time, activityId);
            }
        });

        return view;
    }

    private void openDetailActivity(String...details) {
        Intent intent = new Intent(context, DetailViewActivity.class);
        intent.putExtra("distance", details[0]);
        intent.putExtra("duration", details[1]);
        intent.putExtra("id", details[2]);

        context.startActivity(intent);


    }

    @Nullable
    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }
}
