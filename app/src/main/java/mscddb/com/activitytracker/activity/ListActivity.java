package mscddb.com.activitytracker.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import android.view.MenuItem;

import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import java.util.List;


import mscddb.com.activitytracker.R;
import mscddb.com.activitytracker.adapter.ActivityAdapter;
import mscddb.com.activitytracker.model.Activity;


public class ListActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {



    private static final String TAG = "DetailActivity";
    private Query sortedActivities;

    private ValueEventListener mActivityListener;
    public ChildEventListener mChildEventListener;

    public FirebaseAuth.AuthStateListener authListener;
    private FirebaseUser currentUser;

    private ActivityAdapter adapter;
    private Activity activity;
    private List<Activity> activities;

    ListView lv;
    ArrayList<String> lst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        currentUser = user;

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {

                    startActivity(new Intent(ListActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_list);

        sortedActivities = FirebaseDatabase.getInstance().getReference().child("android").child(currentUser.getUid()).child("activities").orderByChild("timestamp");

        activities = new ArrayList<>();
        lst = new ArrayList<>();


        adapter = new ActivityAdapter(this);
        lv = findViewById(R.id.list_view);
        lv.setAdapter(adapter);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent home = new Intent(ListActivity.this, MainActivity.class);
                    startActivity(home);
                    finish();
                    return true;
                case R.id.navigation_list:
                    return true;
            }
            return false;
        }
    };


    @Override
    public void onStart() {
        super.onStart();

        adapter.notifyDataSetChanged();
        lv.invalidateViews();

        ValueEventListener activityListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(activity == null){
                    activity = new Activity();
                }


                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                    Log.v(TAG,"ACTIVITY ID: "+ childDataSnapshot.getKey()); //displays the key for the node
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                Toast.makeText(ListActivity.this, "Failed to load activity.",
                        Toast.LENGTH_SHORT).show();
            }
        };


        sortedActivities.addValueEventListener(activityListener);


        mActivityListener = activityListener;


        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(TAG, "onChildAdded:" + dataSnapshot.getKey());

                Activity activity = dataSnapshot.getValue(Activity.class);
                activity.setId(dataSnapshot.getKey());
                activities.add(activity);

                Log.d(TAG, "onChildAdded:" + activities);

                String dist = String.valueOf(String.format("%.2f", activity.getDistance()));
                lst.add(dist + " km");

                List<Activity> activitiesArray = activities;
                adapter.setActivities(activitiesArray);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(TAG, "onChildChanged:" + dataSnapshot.getKey());
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        };

        sortedActivities.addChildEventListener(childEventListener);
        mChildEventListener = childEventListener;
    }

    @Override
    public void onStop(){
        super.onStop();
        if (sortedActivities != null) {
            sortedActivities.removeEventListener(mActivityListener);
        }

    }

    @Override
    public void onResume(){
        super.onResume();
        activities.clear();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }


    private class Receiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            supportInvalidateOptionsMenu();
        }
    }
}
