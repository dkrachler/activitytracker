package mscddb.com.activitytracker.model;


/**
 * Created by fantastic4 on 21.01.18.
 */

public class WayPoint {

    private double latitude;
    private double longitude;
    private long timestamp;

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    private double distance;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }



    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public WayPoint() {
        // Default constructor required for calls to DataSnapshot.getValue(WayPoint.class)
    }

    public WayPoint(double latitude, double longitude, long timestamp, double distance) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.timestamp = timestamp;
        this.distance = distance;
    }

}
