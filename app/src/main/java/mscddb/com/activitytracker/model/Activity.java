package mscddb.com.activitytracker.model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by fantastic4 on 21.01.18.
 */

public class Activity {


    private double distance;
    private long duration;
    private List<WayPoint> waypoints;

    public double getMax_speed() {
        return max_speed;
    }

    public void setMax_speed(double max_speed) {
        this.max_speed = max_speed;
    }

    private double max_speed;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    private long timestamp;

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public List<WayPoint> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(List<WayPoint> waypoints) {
        this.waypoints = waypoints;
    }





    public Activity() {
        // Default constructor required for calls to DataSnapshot.getValue(Activity.class)
    }

    public Activity(long duration, double distance, List waypoints, long timestamp, double max_speed) {

        this.duration = duration;
        this.distance = distance;
        this.waypoints = waypoints;
        this.timestamp = timestamp;
        this.max_speed = max_speed;
    }


    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("duration", duration);
        result.put("distance", distance);
        result.put("waypoints", waypoints);
        result.put("timestamp", timestamp);
        result.put("max_speed", max_speed);

        return result;
    }

}
