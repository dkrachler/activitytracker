package mscddb.com.activitytracker.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import mscddb.com.activitytracker.R;
import mscddb.com.activitytracker.model.WayPoint;

public class SummaryActivity extends AppCompatActivity implements OnMapReadyCallback{

    TextView distanceTxt, durationTxt, maxSpeedTxt, speedTxt;

    private FirebaseUser currentUser;
    public FirebaseAuth.AuthStateListener authListener;
    public ValueEventListener mActivityListener;

    public Query lastItemQuery;

    private GoogleMap mMap;
    private List<LatLng> points;

    public mscddb.com.activitytracker.model.Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary_acivity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        currentUser = user;

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    startActivity(new Intent(SummaryActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };


        distanceTxt = findViewById(R.id.distanceDetailTxt);
        durationTxt = findViewById(R.id.durationDetailTxt);
        maxSpeedTxt = findViewById(R.id.maxSpeedDetailTxt);


        lastItemQuery = FirebaseDatabase.getInstance().getReference().child("android").child(currentUser.getUid()).child("activities").limitToLast(1);

        Query ref = lastItemQuery;

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                System.out.println("We're done loading the initial "+dataSnapshot.getChildrenCount()+" items");

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousKey) {


                System.out.println("Add "+dataSnapshot.getKey()+" to UI after "+previousKey);

                activity = new mscddb.com.activitytracker.model.Activity();

                mscddb.com.activitytracker.model.Activity single_activity = dataSnapshot.getValue(mscddb.com.activitytracker.model.Activity.class);
                activity = single_activity;

                System.out.println("Show last activity on data change "+activity.getDuration());

                distanceTxt = findViewById(R.id.distanceDetailTxt);
                durationTxt = findViewById(R.id.durationDetailTxt);
                speedTxt = findViewById(R.id.speedDetailTxt);
                maxSpeedTxt = findViewById(R.id.maxSpeedDetailTxt);

                String distance = String.valueOf(String.format("%.2f", activity.getDistance()));

                long secs = activity.getDuration() / 1000;
                double avgSpeed = (secs > 0)? activity.getDistance() * 1000 / secs * 3.6 : 0;

                String time = String.format("%02d:%02d:%02d", (secs % 86400) / 3600, (secs % 3600) / 60, secs % 60);

                String avg_speed = String.valueOf(String.format("%.2f", avgSpeed));
                String max_speed = String.valueOf(String.format("%.2f", activity.getMax_speed()));

                distanceTxt.setText(distance + " km");
                durationTxt.setText(time);
                speedTxt.setText(avg_speed + " km/h");
                maxSpeedTxt.setText(max_speed + " km/h");


                final List<WayPoint> waypoints = activity.getWaypoints();
                final LatLngBounds.Builder builder = new LatLngBounds.Builder();

                if(waypoints != null) {

                    for (WayPoint waypoint : waypoints) {
                        points.add(new LatLng(waypoint.getLatitude(), waypoint.getLongitude()));
                        builder.include(new LatLng(waypoint.getLatitude(), waypoint.getLongitude()));

                        if (points.size() > 2) {

                            if (initializeMap()) {

                                drawPath(points);
                            }
                        }
                    }



                    mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                        @Override
                        public void onMapLoaded() {
                            if (initializeMap() && points.size() == waypoints.size()) {
                                LatLngBounds bounds = builder.build();
                                int padding = 50; // offset from edges of the map in pixels
                                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                                mMap.animateCamera(cu, 1000, null);
                            }
                        }
                    });

                }


            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }



        });



        points = new ArrayList<>();
        initializeMap();

    }

    private boolean initializeMap() {
        if (mMap == null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mMap = googleMap;
                }
            });
        }
        return (mMap != null);
    }

    @Override
    public void onStart() {
        super.onStart();

        initializeMap();

        ValueEventListener activityListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        lastItemQuery.addValueEventListener(activityListener);
        mActivityListener = activityListener;

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng cLoc = new LatLng(points.get(0).latitude, points.get(0).longitude);

        CameraUpdate center = CameraUpdateFactory.newLatLng(cLoc);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);

        // mMap.moveCamera(center);
        // mMap.animateCamera(zoom);

        if (points.size() > 2) {

            if (initializeMap()) {

                // mMap.moveCamera(center);
                // mMap.animateCamera(zoom);

                drawPath(points);
            }
        }
    }

    public void drawPath(List<LatLng> list) {
        if (initializeMap()) {
            mMap.addPolyline(new PolylineOptions()
                    .addAll(list)
                    .width(10)
                    .color(Color.parseColor("#05b1fb"))
                    .geodesic(true)
            );
        }
    }
}
