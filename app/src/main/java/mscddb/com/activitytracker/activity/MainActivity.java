package mscddb.com.activitytracker.activity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.SystemClock;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import com.google.android.gms.maps.model.PolylineOptions;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mscddb.com.activitytracker.R;
import mscddb.com.activitytracker.constants.Constants;
import mscddb.com.activitytracker.helper.MillisecondsChronometer;
import mscddb.com.activitytracker.model.Activity;
import mscddb.com.activitytracker.model.WayPoint;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener, BottomNavigationView.OnNavigationItemSelectedListener {

    private Button btnStartActivity, btnStopActivity;
    private TextView labelDistance;
    private TextView view_distance;
    private TextView labelDuration;


    private long timestamp;

    private double distance;

    private Location currentLocation;

    LocationManager locationManager;

    private LocationListener locationListener;

    private GoogleMap mMap;
    private List<WayPoint> points;

    private List<LatLng> coords;

    String TAG = "Pos: ";

    MillisecondsChronometer mChronometer;


    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;

    private DatabaseReference mDatabase;

    private Boolean stop;

    private FirebaseUser currentUser;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.logout_button) {
            signOut();
            return true;
        }

        return false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        stop = true;

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);




        BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
                = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_list:

                        if (stop) {
                            Intent list = new Intent(MainActivity.this, ListActivity.class);
                            startActivity(list);
                            finish();
                            return true;
                        } else {
                            Toast.makeText(MainActivity.this, "Active Activity! Please stop Activity first.", Toast.LENGTH_LONG).show();
                        }

                    case R.id.navigation_home:
                        return true;
                }
                return false;
            }
        };


        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        mDatabase = FirebaseDatabase.getInstance().getReference();

        mChronometer = findViewById(R.id.chronometer);



        view_distance = findViewById(R.id.distance);
        view_distance.setText(String.valueOf(distance) + " km");

        labelDuration = findViewById(R.id.labelDuration);
        labelDuration.setText("Duration");

        labelDistance = findViewById(R.id.labelDistance);
        labelDistance.setText("Distance");


        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.app_name));
        setSupportActionBar(toolbar);


        auth = FirebaseAuth.getInstance();
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                currentUser = firebaseAuth.getCurrentUser();

                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };


        btnStartActivity = (Button) findViewById(R.id.start_activity);
        btnStartActivity.setOnClickListener(mStartListener);

        btnStopActivity = (Button) findViewById(R.id.stop_activity);
        btnStopActivity.setOnClickListener(mStopListener);

        btnStopActivity.setVisibility(View.GONE);


        checkLocationPermission();

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10, 100, this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10, 100, this);
        }


        points = new ArrayList<>();


        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (stop) {
                    locationManager.removeUpdates(locationListener);
                    locationManager.removeUpdates(locationListener);
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };



    }

    private void checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            try {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION},
                        Constants.REQUEST_CODE_LOCATION);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void getLocation() {


        String locationProvider = LocationManager.GPS_PROVIDER;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);


        if (lastKnownLocation != null) {
            if (points != null) {

                timestamp = System.currentTimeMillis() / 1000L;

                WayPoint waypoint = new WayPoint(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude(), timestamp, distance);
                points.add(waypoint);
            }
        } else {
            Toast.makeText(this, "Can´t get your location", Toast.LENGTH_LONG).show();
        }

    }


    View.OnClickListener mStartListener = new View.OnClickListener() {
        public void onClick(View v) {
            points = null;
            points = new ArrayList<>();

            coords = null;
            coords = new ArrayList<>();

            mChronometer.setBase(SystemClock.elapsedRealtime());
            mChronometer.start();

            btnStartActivity.setVisibility(View.GONE);
            btnStopActivity.setVisibility(View.VISIBLE);

            distance = 0;

            stop = false;

            listen();
        }
    };

    View.OnClickListener mStopListener = new View.OnClickListener() {
        public void onClick(View v) {

            mChronometer.stop();

            btnStartActivity.setVisibility(View.VISIBLE);
            btnStopActivity.setVisibility(View.GONE);

            writeNewActivity(mChronometer.getTimeElapsed(), distance, points);

            mChronometer.setBase(SystemClock.elapsedRealtime());
            mChronometer.start();
            mChronometer.stop();

            view_distance = findViewById(R.id.distance);
            view_distance.setText("0.0 km");

            stop = true;

            points = null;
            points = new ArrayList<>();

            coords = null;
            coords = new ArrayList<>();

            if(initializeMap()){
                mMap.clear();
            }


            Intent summary = new Intent(MainActivity.this, SummaryActivity.class);
            startActivity(summary);

        }
    };

    private void writeNewActivity(long duration, double distance, List<WayPoint> waypoints) {


        DatabaseReference ref = mDatabase.child("android").child(currentUser.getUid()).child("activities");
        String key = ref.push().getKey();


        timestamp = System.currentTimeMillis() / 1000L;

        List<WayPoint> waypoint_list = new ArrayList<>();
        Double maxSpeed = 0.0;

        for (int i = 0; i < waypoints.size(); i = i + 5) {
            waypoint_list.add(waypoints.get(i));
        }

        for (int e = 0; e < waypoint_list.size() - 1; e++) {


            Double distance_one = waypoint_list.get(e).getDistance();
            Double distance_two = waypoint_list.get(e + 1).getDistance();

            long time_one = waypoint_list.get(e).getTimestamp();
            long time_two = waypoint_list.get(e + 1).getTimestamp();

            long diff_time = time_two - time_one;
            Double diff_distance = distance_two - distance_one;

            Double speed = (diff_distance * 1000) / diff_time * 3.6;

            if (maxSpeed < speed) maxSpeed = speed;
        }


        Log.v(TAG, "SPEEDLIST: " + maxSpeed);

        Activity activity = new Activity(duration, distance, waypoints, timestamp, maxSpeed);

        Map<String, Object> activityValues = activity.toMap();
        Map<String, Object> childUpdates = new HashMap<>();

        childUpdates.put("/android/" + currentUser.getUid() + "/activities/" + key, activityValues);

        mDatabase.updateChildren(childUpdates);


    }

    //sign out method
    public void signOut() {

        auth.signOut();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(authListener);
        getLocation();
    }

    @Override
    public void onStop() {

        super.onStop();
        if (authListener != null) {
            auth.removeAuthStateListener(authListener);
        }

    }


    private void listen() {
        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {


                if (location != null && !stop) {

                    getLocation();

                    currentLocation = location;
                    LatLng cLoc = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());

                    if (initializeMap()) {


                        CameraUpdate center = CameraUpdateFactory.newLatLng(cLoc);
                        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);

                        mMap.moveCamera(center);
                        mMap.animateCamera(zoom);

                        if (points.size() > 2) {
                            WayPoint waypoint = points.get(points.size() - 2);

                            LatLng e = new LatLng(waypoint.getLatitude(), waypoint.getLongitude());
                            Location locationa = new Location("Locatin A");

                            locationa.setLatitude(e.latitude);
                            locationa.setLongitude(e.longitude);

                            distance = distance + locationa.distanceTo(currentLocation) / 1000;

                            view_distance = findViewById(R.id.distance);
                            view_distance.setText(String.valueOf(String.format("%.2f", distance)) + " km");

                            coords.add(new LatLng(waypoint.getLatitude(), waypoint.getLongitude()));

                            drawPath(coords);
                        }
                    }

                    Log.v(TAG, "IN ON LOCATION CHANGE, lat=" + currentLocation.getLatitude() + ", lon=" + currentLocation.getLongitude());
                }

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        currentLocation = locationManager.getLastKnownLocation(LOCATION_SERVICE);
    }

    private boolean initializeMap() {
        if (mMap == null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mMap = googleMap;
                }
            });
        }
        return (mMap != null);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        String locationProvider = LocationManager.GPS_PROVIDER;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);

        if (lastKnownLocation != null) {
            mMap = googleMap;

            LatLng cLoc = new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());

            CameraUpdate center = CameraUpdateFactory.newLatLng(cLoc);
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);

            mMap.moveCamera(center);
            mMap.animateCamera(zoom);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    public void drawPath(List<LatLng> list) {

        if (initializeMap()) {
            Log.v(TAG, "drwaing line " + list);
            mMap.addPolyline(new PolylineOptions()
                    .addAll(list)
                    .width(10)
                    .color(Color.parseColor("#05b1fb"))
                    .geodesic(true)
            );
        }

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
}
